## Strona uniwersytetu

Strona używa:  
- vanilla JS  
- RWD
- EJS (Embedded JavaScript templating)  
- Sass  
- BEM  
- Constraint Valiadation API  
- Google Maps  
- skonfigurowanego przeze mnie Webpacka  
- lekko zmienionego przeze mnie slidera stworzonego w vanilla JS  

Instalacja zależności:  
`npm install`

Uruchamianie serwera deweloperskiego:  
`npm start`

Budowanie wersji produkcyjnej:  
`npm run build`

[Link do strony](http://sandramichalska.pl/strona-uniwersytetu/)
