document.addEventListener('DOMContentLoaded', function() {
    // toggle text input label on text input
    const formInput = document.querySelectorAll(".uw-form__input");

    Object.keys(formInput).map((key) => {
        formInput[key].addEventListener("input", showLabelOnInput)
    });

    function showLabelOnInput() {
        this.value !== ""
            ? this.previousElementSibling.classList.add("uw-form__label--show") 
            : this.previousElementSibling.classList.remove("uw-form__label--show");
    }

    // form validation
    const form  = document.querySelector(".uw-form__form");
    const name = document.querySelector("#name");
    const surname = document.querySelector("#surname");
    const email = document.querySelector("#email");
    const message = document.querySelector("#message");
    const checkbox = document.querySelector("#checkbox");
    const checkboxError = document.querySelector(".uw-form__error--checkbox");
    const nameWrap = document.querySelector(".uw-form__field-wrap--name");
    const surnameWrap = document.querySelector(".uw-form__field-wrap--surname");
    const emailWrap = document.querySelector(".uw-form__field-wrap--email");
    const messageWrap = document.querySelector(".uw-form__field-wrap--message");

    const fieldErrorClass = "uw-form__field-wrap--error";
    const inputErrorClass = "uw-form__input--error";
    const errorMessageClass = "uw-form__error--active";

    let isValidForm;

    function fieldsValidationRealtime(e) {
        if(e.target.validity.valid) {
            e.target.parentElement.classList.remove(fieldErrorClass);

            e.target.nextElementSibling.innerHTML = "";
            e.target.nextElementSibling.classList.remove(errorMessageClass);
        }

        if(!e.target.validity.patternMismatch || !e.target.validity.valueMissing) {
            e.target.nextElementSibling.innerHTML = "";
            e.target.nextElementSibling.classList.remove(errorMessageClass);

            isValidForm = false;
        }

        if(!e.target.validity.valid) {
            e.target.parentElement.classList.add(fieldErrorClass);

            isValidForm = false;
        }
    }

    function emailValidationRealtime(e) {
        if (e.target.validity.valid) {
            e.target.parentElement.classList.remove(fieldErrorClass);
            e.target.classList.remove(inputErrorClass);

            e.target.nextElementSibling.innerHTML = "";
            e.target.nextElementSibling.classList.remove(errorMessageClass);
        } 
        
        if(!e.target.validity.valueMissing) {
            e.target.nextElementSibling.innerHTML = "";
            e.target.nextElementSibling.classList.remove(errorMessageClass);
        }
        
        if(!e.target.validity.valid) {
            e.target.parentElement.classList.add(fieldErrorClass);
            e.target.classList.add(inputErrorClass);
        }
    }

    function fieldsValidationSubmit(field, fieldWrap) {
        if(field.validity.valid) {
            fieldWrap.classList.remove(fieldErrorClass);
        } else {
            if(field.validity.patternMismatch) {
                field.nextElementSibling.innerHTML = "Pole zawiera niedozwolone znaki";
                field.nextElementSibling.classList.add(errorMessageClass);

                isValidForm = false;
            }

            if(field.validity.valueMissing) {
                fieldWrap.classList.add(fieldErrorClass);

                field.nextElementSibling.innerHTML = "Pole nie może być puste";
                field.nextElementSibling.classList.add(errorMessageClass);

                isValidForm = false;
            }

            isValidForm = false;
        }
    }

    function emailValidationSubmit() {
        if (email.validity.valid) {
            emailWrap.classList.remove(fieldErrorClass);
            email.classList.remove(inputErrorClass);
        } else {
            emailWrap.classList.add(fieldErrorClass);
            email.classList.add(inputErrorClass);

            if(email.validity.valueMissing) {
                email.nextElementSibling.innerHTML = "Pole nie może być puste";
                email.nextElementSibling.classList.add(errorMessageClass);

                isValidForm = false;
            }

            if(email.validity.typeMismatch) {   
                email.nextElementSibling.innerHTML = "Wpisany adres jest niepoprawny";
                email.nextElementSibling.classList.add(errorMessageClass);

                isValidForm = false;
            }

            isValidForm = false;
        }
    }
    
    function checkboxValidationSubmit() {
        if(checkbox.validity.valueMissing) {
            checkboxError.innerHTML = "Zgoda jest wymagana";
            checkboxError.classList.add(errorMessageClass);

            isValidForm = false;
        } else {
            checkboxError.innerHTML = "";
            checkboxError.classList.remove(errorMessageClass);
        }
    }

    name.addEventListener("input", fieldsValidationRealtime, false);
    surname.addEventListener("input", fieldsValidationRealtime, false);
    message.addEventListener("input", fieldsValidationRealtime, false);
    email.addEventListener("input", emailValidationRealtime, false);

    form.addEventListener("submit", e => {
        isValidForm = true;

        fieldsValidationSubmit(name, nameWrap);
        fieldsValidationSubmit(surname, surnameWrap);
        fieldsValidationSubmit(message, messageWrap);
        emailValidationSubmit();
        checkboxValidationSubmit();

        if(!isValidForm) e.preventDefault();
    }, false);
}, false);