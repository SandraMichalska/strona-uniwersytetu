  'use strict';

  // utility functions; IE9 doesn't support classList
  function hasClass(el, className) {  
    return el.classList 
      ? el.classList.contains(className) 
      : new RegExp('(^| )' + className + '( |$)', 'gi').test(el.className);
  }

  function addClass(el, className) {
    if (el.classList) {
      el.classList.add(className);
    } else {
      el.className += ' ' + className;
    }
  }

  function removeClass(el, className) {
    if (el.classList) {
      el.classList.remove(className);
    } else {
      el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
    }
  }

  // utility fct to combine the default settings, and the settings passed in at initialization
  function extendObj(def, newSettings) {
    if (typeof newSettings !== "undefined") {
      for (var prop in def) {
        if (newSettings[prop] !== undefined) {
          def[prop] = newSettings[prop];
        }
      }
    }
  }

  var sliderPlugin = (function () {
    var mySlider = function (settings) {

      // always loop
      this.def = {
        target: document.querySelector('.uw-slider'),
        dotsWrapper: document.querySelector('.uw-dots'),
        arrowLeft: document.querySelector('.uw-arrow-left'),
        arrowRight: document.querySelector('.uw-arrow-right'),
        transition: {
          speed: 1000,
          easing: ''
        },
        swipe: true,
        autoHeight: true,
        afterChangeSlide: function afterChangeSlide() { }
      }

      extendObj(this.def, settings);
      
      // initialize the slider
      this.init();
    }

    mySlider.prototype.buildDots = function () {
      var that = this;

      for (var i = 0; i < this.totalSlides; i++) {
        var dot = document.createElement('li');
        addClass(dot, 'uw-dots__dot');
        dot.setAttribute('data-slide', i + 1);
        this.def.dotsWrapper.appendChild(dot);
      }

      this.def.dotsWrapper.addEventListener('click', function (e) {
        if (e.target && e.target.nodeName == "LI") {
          that.curSlide = e.target.getAttribute('data-slide');
          that.gotoSlide();
        }
      }, false);
    }

    mySlider.prototype.getCurLeft = function () {
      this.curLeft = parseInt(this.sliderInner.style.left.split('px')[0]);
    }
    
    mySlider.prototype.gotoSlide = function () {
      var that = this;

      if(that.sliderInner.style.transition === '') { // line added to keep from clicking a dot when slides are changing
        this.sliderInner.style.transition = 'left ' + this.def.transition.speed / 1000 + 's ' + this.def.transition.easing;
        this.sliderInner.style.left = -this.curSlide * this.slideW + 'px';
        addClass(this.def.target, 'isAnimating');

        setTimeout(function () {
          that.sliderInner.style.transition = '';
          removeClass(that.def.target, 'isAnimating');
        }, this.def.transition.speed);

        this.setDot();
        
        if (this.def.autoHeight) {
          this.def.target.style.height = this.allSlides[this.curSlide].offsetHeight + "px";
        }

        this.def.afterChangeSlide(this);
      }
    }

    mySlider.prototype.init = function () {
      var that = this;

      function onResize(c, t) {
        onresize = function () {
          clearTimeout(t);
          t = setTimeout(c, 100);
        }
        return onresize;
      }

      function loadedImg(el) {
        var loaded = false;

        function loadHandler() {
          if (loaded) {
            return;
          }
          loaded = true;
          that.loadedCnt++;

          if (that.loadedCnt >= that.totalSlides + 2) {
            that.updateSliderDimension();
          }
        }

        var img = el.querySelector('img');
        if (img) {
          img.onload = loadHandler;
          img.src = img.getAttribute('data-src');
          img.style.display = 'block';
          if (img.complete) {
            loadHandler();
          }
        } else {
          that.updateSliderDimension();
        }
      }

      window.addEventListener("resize", onResize(function () {
        that.updateSliderDimension();
      }), false);

      var nowHTML = this.def.target.innerHTML;
      this.def.target.innerHTML = '<div class="uw-slider__inner">' + nowHTML + '</div>';

      this.allSlides = 0;
      this.curSlide = 0;
      this.curLeft = 0;
      this.totalSlides = this.def.target.querySelectorAll('.uw-slider__slide').length;

      this.sliderInner = this.def.target.querySelector('.uw-slider__inner');
      this.loadedCnt = 0;

      // append clones
      var cloneFirst = this.def.target.querySelectorAll('.uw-slider__slide')[0].cloneNode(true);
      this.sliderInner.appendChild(cloneFirst);
      var cloneLast = this.def.target.querySelectorAll('.uw-slider__slide')[this.totalSlides - 1].cloneNode(true);
      this.sliderInner.insertBefore(cloneLast, this.sliderInner.firstChild);

      this.curSlide++;
      this.allSlides = this.def.target.querySelectorAll('.uw-slider__slide');

      this.sliderInner.style.width = (this.totalSlides + 2) * 100 + "%";
      for (var i = 0; i < this.totalSlides + 2; i++) {
        this.allSlides[i].style.width = 100 / (this.totalSlides + 2) + "%";
        loadedImg(this.allSlides[i]);
      }

      this.buildDots();
      this.setDot();
      this.initArrows();

      function addListenerMulti(el, s, fn) {
        s.split(' ').forEach(function (e) {
          return el.addEventListener(e, fn, false);
        });
      }

      function removeListenerMulti(el, s, fn) {
        s.split(' ').forEach(function (e) {
          return el.removeEventListener(e, fn, false);
        });
      }

      if (this.def.swipe) {
        addListenerMulti(this.sliderInner, 'mousedown touchstart', swipeStart);
      }

      this.isAnimating = false;

      function swipeStart(e) {
        var touch = e;

        that.getCurLeft();  
        if (!that.isAnimating) {
          if (e.type == 'touchstart') {
            touch = e.targetTouches[0] || e.changedTouches[0];
          }
          that.startX = touch.pageX;
          that.startY = touch.pageY;
          addListenerMulti(that.sliderInner, 'mousemove touchmove', swipeMove);
          addListenerMulti(document.querySelector('body'), 'mouseup touchend', swipeEnd);
        }
      }

      function swipeMove(e) {
        var touch = e;

        if (e.type == 'touchmove') {
          touch = e.targetTouches[0] || e.changedTouches[0];
        }
        that.moveX = touch.pageX;
        that.moveY = touch.pageY;

        // for scrolling up and down
        if (Math.abs(that.moveX - that.startX) < 40) return;

        that.isAnimating = true;
        addClass(that.def.target, 'isAnimating');
        e.preventDefault();

        if (that.curLeft + that.moveX - that.startX > 0 && that.curLeft == 0) {
          that.curLeft = -that.totalSlides * that.slideW;
          that.curSlide = 4; // line added to fix a bug with slides
          
        } else if (that.curLeft + that.moveX - that.startX < -(that.totalSlides + 1) * that.slideW) {
          that.curLeft = -that.slideW;
          that.curSlide = 1; // line added to fix a bug with slides
        }
        that.sliderInner.style.left = that.curLeft + that.moveX - that.startX + "px";
      }

      function swipeEnd(e) {
        var touch = e;
        that.getCurLeft();

        if (Math.abs(that.moveX - that.startX) === 0) return;

        that.stayAtCur = Math.abs(that.moveX - that.startX) < 40 || typeof that.moveX === "undefined" ? true : false;
        that.dir = that.startX < that.moveX ? 'left' : 'right';

        if (that.stayAtCur) { } else {
          that.dir == 'left' ? that.curSlide-- : that.curSlide++;
          if (that.curSlide < 0) {          
            that.curSlide = that.totalSlides;
          } else if (that.curSlide == that.totalSlides + 2) {
            that.curSlide = 1;
          }
        }

        that.gotoSlide();

        delete that.startX;
        delete that.startY;
        delete that.moveX;
        delete that.moveY;

        that.isAnimating = false;
        removeClass(that.def.target, 'isAnimating');
        removeListenerMulti(that.sliderInner, 'mousemove touchmove', swipeMove);
        removeListenerMulti(document.querySelector('body'), 'mouseup touchend', swipeEnd);
      }
    }

    mySlider.prototype.initArrows = function () {
      var that = this;

      if (this.def.arrowLeft != '') {
        this.def.arrowLeft.addEventListener('click', function () {
          if (!hasClass(that.def.target, 'isAnimating')) {
            if (that.curSlide == 1) {
              that.curSlide = that.totalSlides + 1;
              that.sliderInner.style.left = -that.curSlide * that.slideW + 'px';
            }

            // fix error with changing slides with arrows and then sliding
            if (that.curSlide < 1) {
              that.curSlide = that.totalSlides;
              that.sliderInner.style.left = -that.curSlide * that.slideW + 'px';
            }

            that.curSlide--;
            setTimeout(function () {
              that.gotoSlide();
            }, 20);
          }
        }, false);
      }

      if (this.def.arrowRight != '') {
        this.def.arrowRight.addEventListener('click', function () {
          if (!hasClass(that.def.target, 'isAnimating')) {
            if (that.curSlide == that.totalSlides) {
              that.curSlide = 0;
              that.sliderInner.style.left = -that.curSlide * that.slideW + 'px';
            }

            // fix error with changing slides with arrows and then sliding
            if (that.curSlide > that.totalSlides) {
              that.curSlide = 1;
              that.sliderInner.style.left = -that.curSlide * that.slideW + 'px';
            }

            that.curSlide++;
            setTimeout(function () {
              that.gotoSlide();
            }, 20);
          }
        }, false);
      }
    }

    mySlider.prototype.setDot = function () {
      var tarDot = this.curSlide - 1;

      for (var j = 0; j < this.totalSlides; j++) {
        removeClass(this.def.dotsWrapper.querySelectorAll('.uw-dots__dot')[j], 'active');
      }

      if (this.curSlide - 1 < 0) {
        tarDot = this.totalSlides - 1;
      } else if (this.curSlide - 1 > this.totalSlides - 1) {
        tarDot = 0;
      }
      addClass(this.def.dotsWrapper.querySelectorAll('.uw-dots__dot')[tarDot], 'active');
    }
    
    mySlider.prototype.updateSliderDimension = function () {
      this.slideW = parseInt(this.def.target.querySelectorAll('.uw-slider__slide')[0].offsetWidth);
      this.sliderInner.style.left = -this.slideW * this.curSlide + "px";

      if (this.def.autoHeight) {
        this.def.target.style.height = this.allSlides[this.curSlide].offsetHeight + "px";
      } else {
        for (var i = 0; i < this.totalSlides + 2; i++) {
          if (this.allSlides[i].offsetHeight > this.def.target.offsetHeight) {
            this.def.target.style.height = this.allSlides[i].offsetHeight + "px";
          }
        }
      }
      this.def.afterChangeSlide(this);
    }
    
    return mySlider;
  })();

  new sliderPlugin({
    target: document.querySelector('.uw-slider'),
    dotsWrapper: document.querySelector('.uw-dots'),
    arrowLeft: document.querySelector('.uw-arrow-left'),
    arrowRight: document.querySelector('.uw-arrow-right')
  });
