document.addEventListener('DOMContentLoaded', function() {
  // sticky menu
  const menu = document.querySelector(".uw-menu");
  const headerMenulist = document.querySelector(".uw-menu__list");
  const nav = document.querySelector(".uw-header__nav");

  window.addEventListener("scroll", stickyMenu);

  function stickyMenu() {
    if (window.pageYOffset >= nav.offsetHeight) {
      menu.classList.add("uw-menu--sticky");
      headerMenulist.classList.add("uw-menu__list--sticky");
    } else {
      menu.classList.remove("uw-menu--sticky");
      headerMenulist.classList.remove("uw-menu__list--sticky");
    }
  }


  // scroll to next section on button click
  const button = document.querySelector(".uw-kv__button-link--1");
  let nextSection = document.querySelector(".uw-forwhom");

  button.addEventListener("click", scrollToSecondSection);

  function scrollToSecondSection() {
    let scrollContainer = nextSection;
    let nextSectionY = 0;
    
    do {
      scrollContainer = scrollContainer.parentNode;
      if (!scrollContainer) return;
      scrollContainer.scrollTop += 1;
    } while (scrollContainer.scrollTop === 0);

    do {
      if (nextSection === scrollContainer) break;
      nextSectionY += nextSection.offsetTop;
    } while (nextSection === nextSection.offsetParent);

    const scroll = (c, a, b, i) => {
      i++;
      if (i > 140) return;
      c.scrollTop = a + (b - a) / 140 * i;

      setTimeout(() => {
        scroll(c, a, b, i); 
      }, 1);
    }

    scroll(scrollContainer, scrollContainer.scrollTop, nextSectionY, 0);
  }
});