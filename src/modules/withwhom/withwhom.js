document.addEventListener('DOMContentLoaded', function() {
    // Video modal
    const videoImg = document.querySelector(".uw-withwhom__img-wrap");
    const hideModalX = document.querySelector(".uw-modal__hide-x");
    const iframe = document.querySelector(".uw-modal__iframe");
    const body = document.querySelector("body");
    const modal = document.querySelector(".uw-modal");

    videoImg.addEventListener("click", showModal);
    hideModalX.addEventListener("click", hideModal);

    function showModal(e) {
        if (e.target.nodeName == "IMG") {
            iframe.setAttribute("src", "https://www.youtube.com/embed/iEM9yyHh1Ys");
            body.classList.add("uw-no-scroll");
            modal.classList.add("uw-modal--show");
        }
    }
    
    function hideModal() {
        iframe.setAttribute("src", "");
        body.classList.remove("uw-no-scroll");
        modal.classList.remove("uw-modal--show");
    }
});