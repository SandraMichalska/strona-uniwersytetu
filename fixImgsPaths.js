// fix Webpack's problem with a slash at paths in CSS properties, e.g. background: #fff url("/static/img/tick.png")
// remove slash at the beginning of the path to make images work in production
// example: change url("/static/img/tick.png") to url("static/img/tick.png")

const fs = require('fs')

fs.readFile("./dist/index.css", 'utf8', function (err, data) {
  if (err) return console.log(err);

  const result = data.replace(/\"\/static/g, '"static');

  fs.writeFile("./dist/index.css", result, 'utf8', function (err) {
     if (err) return console.log(err);
  });
});